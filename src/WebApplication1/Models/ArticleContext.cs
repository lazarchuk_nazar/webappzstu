﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class ArticleContext: DbContext
    {
        DbSet<Article> Articles { get; set; }

        public ArticleContext(DbContextOptions<ArticleContext> options)
            : base(options)
        {

        }
    }
}
